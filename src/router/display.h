
#define MAX_DISPLAY_WIDTH 1024
#define MAX_HEIGHT_STATUS 1024

void display_status_on(int on);
void display_status_start(void);
void display_status_line(const char *from_if, int from_count, const char *from_id, int to_count, const char *to_if, const char *to_id, enum call_state to_state);
void display_status_end(void);

