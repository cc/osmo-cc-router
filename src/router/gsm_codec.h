
int gsm_fr_create(call_relation_t *relation);
void gsm_fr_destroy(call_relation_t *relation);
void gsm_fr_encode(uint8_t *src_data, int src_len, uint8_t **dst_data, int *dst_len, void *priv);
void gsm_fr_decode(uint8_t *src_data, int src_len, uint8_t **dst_data, int *dst_len, void *priv);

