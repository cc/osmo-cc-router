
struct string_queue {
	struct string_queue	*next;
	char			*string;
};

struct call;

typedef struct routing {
	struct call		*call;

	int			routing;	/* set, if routing is performed (script runs) */

	int			envc;		/* number of environment variables */
	const char		*envp[256];	/* environment variables */
	int			envc_dialing;	/* envc index for dialing number */
	int			envc_keypad;	/* envc index for keypad */
	int			envc_complete;	/* envc index for complete */

	pid_t			script_pid;	/* pid of routing script */
	struct osmo_fd		script_stdin;	/* pipe to stdin */
	struct osmo_fd		script_stdout;	/* pipe from stdout */
	int			script_stdout_child;	/* child side of pipe */
	struct osmo_fd		script_stderr;	/* pipe from stderr */
	int			script_stderr_child;	/* child side of pipe */

	struct string_queue	*stdin_queue;	/* strings to write to script */
	char			stdout_buffer[65536]; /* line buffer when reading from script */
	int			stdout_pos;	/* number of characters in buffer */
	struct string_queue	*stdout_queue;	/* strings read from script */
	char			stderr_buffer[65536]; /* line buffer when reading from script */
	int			stderr_pos;	/* number of characters in buffer */
	struct string_queue	*stderr_queue;	/* strings read from script */
} routing_t;

void env_help(void);
void routing_env_msg(routing_t *routing, osmo_cc_msg_t *msg);
void routing_env_dialing(routing_t *routing, char *number, char *keypad, int complete);
void routing_env_free(routing_t *routing);
int routing_start(routing_t *routing, const char *script, const char *shell);
void routing_stop(routing_t *routing);
void routing_send(routing_t *routing, const char *string);
int routing_handle(routing_t *routing);

/* callbacks */
void routing_receive_stdout(routing_t *routing, const char *string);
void routing_receive_stderr(routing_t *routing, const char *string);
void routing_close(routing_t *routing);

