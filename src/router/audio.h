
void audio_init(void);
void receive_originator(struct osmo_cc_session_codec *codec, uint8_t marker, uint16_t sequence_number, uint32_t timestamp, uint32_t ssrc, uint8_t *data, int len);
void receive_terminator(struct osmo_cc_session_codec *codec, uint8_t marker, uint16_t sequence_number, uint32_t timestamp, uint32_t ssrc, uint8_t *data, int len);
void tx_telephone_event(call_relation_t *relation, uint8_t marker, struct telephone_event *te);
void call_clock(int len);
void encode_l16(uint8_t *src_data, int src_len, uint8_t **dst_data, int *dst_len, void *priv);
void decode_l16(uint8_t *src_data, int src_len, uint8_t **dst_data, int *dst_len, void *priv);
void encode_te(uint8_t *src_data, int src_len, uint8_t **dst_data, int *dst_len, void *priv);
void decode_te(uint8_t *src_data, int src_len, uint8_t **dst_data, int *dst_len, void *priv);

