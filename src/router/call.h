
#include <osmocom/core/timer.h>
#include <osmocom/core/select.h>
#include <osmocom/cc/endpoint.h>
#include <osmocom/cc/helper.h>
#include "../libsample/sample.h"
#include "../libjitter/jitter.h"
#include "../libsendevolumenregler/sendevolumenregler.h"
#include "../libwave/wave.h"
#include "../libdtmf/dtmf_decode.h"


enum call_state {
	CALL_STATE_IDLE = 0,
	CALL_STATE_SETUP,
	CALL_STATE_OVERLAP,
	CALL_STATE_PROCEEDING,
	CALL_STATE_ALERTING,
	CALL_STATE_CONNECT,
	CALL_STATE_DISC_FROM_ORIG,
	CALL_STATE_DISC_FROM_TERM,
};

#include "routing.h"

#define MAX_CODECS	8

/* relation to upper layer */
typedef struct call_relation {
	struct call_relation	*next;
	int			num;		/* number counter for debugging */
	struct call		*call;		/* points to the call */
	osmo_cc_endpoint_t	*cc_ep;		/* what endpoint is used for this relation */
	uint32_t		cc_callref;	/* callref for each relation */
	const char		*sdp;		/* received SDP? */
	int			tones_recv;	/* are inband tones available? */

	char			interface[256];	/* interface */
	char			id[256];	/* caller ID / dialing*/
	enum call_state		state;		/* state for status display */

	void			*gsm_fr_encoder;/* instance for FR codec */
	void			*gsm_fr_decoder;

	osmo_cc_session_t	*cc_session;
	int			codec_negotiated;
	struct osmo_cc_helper_audio_codecs orig_codecs[MAX_CODECS + 1];	/* codecs for originator */
	struct osmo_cc_helper_audio_codecs term_codecs[MAX_CODECS + 1]; /* codecs for terminator, stored at relation of originator */
	osmo_cc_session_codec_t	*codec, *telephone_event; /* codec and optional telephone event codec */

	dtmf_dec_t		dtmf_dec;	/* dtmf decoder */
	int			dtmf_dec_enable;/* feed decoder with data */
	int			te_started;	/* we got a digit via telephone-event */
} call_relation_t;

/* call instance */
typedef struct call {
	struct call		*next;
	int			num;		/* number counter for debugging */

	/* call */
	enum call_state		state;		/* current state of call */
	osmo_cc_msg_t		*setup_msg;	/* stored setup message for later IE forwarding */
	char			dialing_number[256]; /* accumulated dial string (setup + info) */
	char			dialing_keypad[256]; /* accumulated keypad string (setup + info) */
	int			forking;	/* set, if call is forked (started with 2 or more calls) */
	uint8_t			collect_cause;	/* cause from forking calls */
	call_relation_t		*relation_list;	/* list of all upper layer relations */
	/* NOTE: the first relation is always the originator */
	int			tones_sent;	/* did we announce inband tones? */
	int			sdp_forwarded;	/* if set, we cannot do RTP-Proxy anymore */

	/* routing */
	routing_t		routing;

	/* audio */
	int			rtp_proxy;
	double			tx_gain;	/* gain of what is received from originator */
	double			rx_gain;	/* gain of what is transmitted to the originator */
	int			tx_compress;	/* use compressor originator->terminator */
	int			rx_compress;	/* use compressor terminator->originator */
	sendevolumenregler_t	tx_compressor;
	sendevolumenregler_t	rx_compressor;
	jitter_t		orig_dejitter;	/* jitter buffer for RTP proxy (originating source) */
	jitter_t		term_dejitter;	/* jitter buffer for RTP proxy (terminating source) */

	/* metering */
	int			metering_data_valid;	/* metering data provided, but not transmitted yet */
	uint16_t	metering_connect_units;	/* units to be charged at connect state */
	struct timeval	metering_unit_period;	/* period of one metering unit */

	/* wave playback/record */
	wave_play_t		orig_play;		/* play a wave file */
	int			orig_play_loop;	/* set to play loop */
	char			orig_play_filename[256];/* stored for reopen on loop */
	double			orig_play_deviation;	/* stored for reopen on loop */
	wave_play_t		term_play;		/* play a wave file */
	int			term_play_loop;	/* set to play loop */
	char			term_play_filename[256];/* stored for reopen on loop */
	double			term_play_deviation;	/* stored for reopen on loop */
	wave_rec_t		rec;		/* record a wave file */
} call_t;

extern call_t *call_list;

struct telephone_event {
	uint8_t	event;
	uint8_t e, r;
	uint8_t volume;
	uint16_t duration;
};

int call_init(osmo_cc_endpoint_t *cc_ep1, osmo_cc_endpoint_t *cc_ep2, const char *_routing_script, const char *_routing_shell);
void call_exit(void);
int call_handle(void);
void cc_message(osmo_cc_endpoint_t *ep, uint32_t callref, osmo_cc_msg_t *msg);
void rx_telephone_event(call_relation_t *relation, uint8_t marker, struct telephone_event *te, int called);

void routing_help(void);

