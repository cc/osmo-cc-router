#include <errno.h>
#include <stdint.h>
#include <stdlib.h>

#include <gsm.h>
#include "call.h"
#include "gsm_codec.h"
#include "../liblogging/logging.h"


/* create gsm instance */
int gsm_fr_create(call_relation_t *relation)
{
	int value = 1;
	gsm encoder, decoder;

	encoder = gsm_create();
	if (!encoder) {
		fprintf(stderr, "gsm codec failed to intialize.\n");
		abort();
	}
	gsm_option(encoder, 0/*GSM_OPT_WAV49*/, &value);

	decoder = gsm_create();
	if (!decoder) {
		fprintf(stderr, "gsm codec failed to intialize.\n");
		abort();
	}
	gsm_option(decoder, 0/*GSM_OPT_WAV49*/, &value);

	relation->gsm_fr_encoder = encoder;
	relation->gsm_fr_decoder = decoder;

	return -ENOMEM;
}

/* free gsm instance */
void gsm_fr_destroy(call_relation_t *relation)
{
	gsm_destroy((gsm)relation->gsm_fr_encoder);
	gsm_destroy((gsm)relation->gsm_fr_decoder);
}

/* encode samples into frame */
void gsm_fr_encode(uint8_t *src_data, int src_len, uint8_t **dst_data, int *dst_len, void *priv)
{
	call_relation_t *relation = (call_relation_t *)priv;
	gsm_signal *src = (gsm_signal *)src_data;
	gsm_byte *dst;

	dst = malloc(33);
        if (!dst)
		return;

	if (src_len / 2 != 160)
		fprintf(stderr, "GSM encoder requires 160 samples per chunk! Please fix!\n");
	else
		gsm_encode((gsm)relation->gsm_fr_encoder, src, dst);
	*dst_data = (uint8_t *)dst;
	*dst_len = 33;
}

/* decode frame into samples */
void gsm_fr_decode(uint8_t *src_data, int src_len, uint8_t **dst_data, int *dst_len, void *priv)
{
	call_relation_t *relation = (call_relation_t *)priv;
	gsm_byte *src = (gsm_byte *)src_data;
	gsm_signal *dst;

	dst = malloc(160 * 2);
        if (!dst)
		return;

	if (src_len != 33)
		fprintf(stderr, "GSM decoder requires 33 bytes per chunk! Please fix!\n");
	else
		gsm_decode((gsm)relation->gsm_fr_decoder, src, dst);
	*dst_data = (uint8_t *)dst;
	*dst_len = 160 * 2;
}

