
enum {
	DLCC,
	DOPTIONS,
	DJITTER,
	DPH,
	DWAVE,
	DROUTER,
	DSTDERR,
};

#define DLCC_DEFINED

extern struct log_info_cat log_categories[];
extern size_t log_categories_size;

